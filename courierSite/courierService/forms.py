from django.forms import ModelForm
from courierService.models import State, City, Customer, Booking, Payment

class SignupForm(ModelForm):
    class Meta:
        model = Customer
        fields = ['customer_name', 'customer_address', 'customer_email', 'state', 'city', 'mobile_number', 'pan', 'password']
    
class LoginForm(ModelForm):
    class Meta:
        model = Customer
        fields = ['customer_name', 'password']

class BookingForm(ModelForm):
    class Meta:
        model = Booking
        fields = ['sender_name', 'sender_address', 'sender_phone_number', 'sender_email', 'sender_state', 'sender_city', 'sender_area', 'reciever_name', 'reciever_state' ,'reciever_city', 'reciever_area', 'reciever_address' , 'package_type' , 'volume', 'height', 'charged_weight', 'status']


class PaymentForm(ModelForm):
    class Meta:
        model = Payment
        fields = ['booking_number', 'ftl_charge', 'docket_charge', 'miscellaneous_charge', 'gst', 'total_charge', 'total_amount', 'total_amount', 'amount_pending' ,'payment_option', 'status']
