from django.apps import AppConfig


class CourierserviceConfig(AppConfig):
    name = 'courierService'
