from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from .forms import SignupForm, LoginForm, BookingForm, PaymentForm

# Create your views here.

def index(request):
   context = {
       'testing_val' : "This is the latest index testing",
   }
   return render(request, 'courierService/index.html', context)

def contact(request):
    return render(request, 'courierService/contact.html', {})

#def signup(request):
 #   return render(request, 'courierService/signup.html', {})

def login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            form = LoginForm()
            form.save()
            return HttpResponse('Thank you for signing up and enjoy our services...')
        else :
            return render(request, 'courierService/signup.html', {'form': form})
    else:
        form = LoginForm()
    return render(request, 'courierService/signup.html', {'form': form})

def signup(request):
    if request.method == 'POST':
        form = SignupForm(request.POST)
        if form.is_valid():
            form = SignupForm()
            form.save()
            return HttpResponse('Thank you for signing up and enjoy our services...')
        else :
            return render(request, 'courierService/signup.html', {'form': form})
    else:
        form = SignupForm()
    return render(request, 'courierService/login.html', {'form': form})

def booking(request):
    if request.method == 'POST':
        form = BookingForm(request.POST)
        if form.is_valid():
            form = BookingForm()
            form.save()
            return HttpResponse('Thank you for signing up and enjoy our services...')
        else :
            return render(request, 'courierService/signup.html', {'form': form})
    else:
        form = BookingForm()
    return render(request, 'courierService/booking.html', {'form': form})

def payment(request):
    if request.method == 'POST':
        form = PaymentForm(request.POST)
        if form.is_valid():
            form = PaymentForm()
            form.save()
            return HttpResponse('Thank you for signing up and enjoy our services...')
        else:
            return render(request, 'courierService/signup.html', {'form': form})
    else:
        form = PaymentForm()
    return render(request, 'courierService/booking.html', {'form': form})