from django.db import models
import datetime
from django.utils import timezone
from django.core.validators import RegexValidator

#models class to be added from here

class City(models.Model):
    city_name = models.CharField(max_length=60)

class State(models.Model):
    state_name = models.CharField(max_length=60)

class Area(models.Model):
    area_name = models.CharField(max_length=200)

class Customer(models.Model):
    customer_name = models.CharField(max_length=60, null=False, blank=False)
    customer_address = models.TextField(max_length=400, null=False, blank=False)
    state = models.ForeignKey(State, on_delete=models.CASCADE, null=False, blank=False)
    city = models.ForeignKey(City, on_delete=models.CASCADE, null=False, blank=False)
    phone_regex = RegexValidator(regex=r'^[0-9]{10}', message="Phone number must be entered in the format: '999999999'. Up to 12 digits allowed.")
    mobile_number = models.CharField(validators=[phone_regex], max_length=13, null=False, blank=False, unique=True) # validators should be a list
    customer_email = models.EmailField(max_length=254, null=False, blank=False)
    pan = models.CharField(validators=[RegexValidator(r'^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$', message='PAN length has to be 10')],max_length=10, null=False, blank=False, unique=True)
    password = models.CharField(validators=[RegexValidator(r'^(\w+\d+|\d+\w+)+$]', message='Password should be a combination of alphabets and numbers ')], max_length=40, null=False, blank=False)
    customer_email = models.EmailField('Sender Email', max_length=254, null=False, blank=False)
    
class Booking(models.Model):
    sender_name = models.CharField('Sender name',max_length=60, null=False, blank=False)
    sender_address = models.TextField('Sender address', max_length=400, null=False, blank=False)
    phone_regex = RegexValidator(regex=r'^[0-9]{10}', message="Phone number must be entered in the format: '999999999'. Up to 12 digits allowed.")
    sender_phone_number = models.CharField('Phone Number', validators=[phone_regex], max_length=13, null=False, blank=False, unique=True) # validators should be a list
    sender_email = models.EmailField('Sender Email', max_length=254, null=False, blank=False)
    sender_state = models.ForeignKey(State, related_name='%(class)s_requests_created',on_delete=models.CASCADE, null=False, blank=False)
    sender_city = models.ForeignKey(City, related_name='%(class)s_requests_created', on_delete=models.CASCADE, null=False, blank=False)
    sender_area = models.ForeignKey(Area, related_name='%(class)s_requests_created', on_delete=models.CASCADE, null=False, blank=False)
    reciever_name = models.CharField('Reciever name', max_length=60, null=False, blank=False)
    reciever_state = models.ForeignKey(State, on_delete=models.CASCADE, null=False, blank=False)
    reciever_city = models.ForeignKey(City, on_delete=models.CASCADE, null=False, blank=False)
    reciever_area = models.ForeignKey(Area, on_delete=models.CASCADE, null=False, blank=False)
    reciever_address = models.TextField('Reciever address', max_length=400, null=False, blank=False)
    package_type = models.CharField('Package type', max_length=200)
    volume = models.DecimalField(max_digits=5, decimal_places=3)
    height = models.DecimalField(max_digits=5, decimal_places=3)
    charged_weight = models.DecimalField(max_digits=5, decimal_places=3)
    client_code = models.ForeignKey(Customer, null=False, on_delete=models.CASCADE)
    status = models.CharField('Status',max_length=200, null=False, blank=False)
    booking_time = models.DateTimeField('Booking date')
    delivary_time = models.DateTimeField('Estimated delivary time')

    def current_booking_date(self):
        now = timezone.now()
        return now

    def estimated_delivary_date(self):
        now = timezone.now()
        return now + datetime.timedelta(days=7)>self.booking_time

class Payment(models.Model):
    GST_TYPES = (
        ('1', 5),
        ('2', 12),
        ('3', 18)
    )

    PAYMENT_OPTION = (
        ('1', 'Cash'),
        ('2', 'Net Banking'),
        ('3', 'Credit card'),
        ('4', 'Debit card')
    )

    PAY_STATUS = (
        ('1', 'Pending'),
        ('2', 'Complete')
    )

    booking_number = models.ForeignKey(Booking, on_delete=models.CASCADE)
    ftl_charge = models.DecimalField(max_digits=5, decimal_places=3, default=0)
    docket_charge = models.DecimalField(max_digits=5, decimal_places=3)
    miscellaneous_charge = models.DecimalField(max_digits=5, decimal_places=3)
    gst = models.IntegerField(choices=GST_TYPES)
    total_charge = models.DecimalField(max_digits=5, decimal_places=3)
    total_amount = models.DecimalField(max_digits=5, decimal_places=3)
    amount_paid = models.DecimalField(max_digits=5, decimal_places=3)
    amount_pending = models.DecimalField(max_digits=5, decimal_places=3)
    payment_option = models.CharField(max_length=200, choices=PAYMENT_OPTION)
    status = models.CharField(max_length=200, choices=PAY_STATUS)
    
    
class Vehicle(models.Model):

    AVAIL_STATUS = (
        ('Available', 'Available'),
        ('Unavailable', 'Unavailable')
    )
    PAYMENT_OPTION = (
        ('Cash', 'Cash'),
        ('Net Banking', 'Net Banking'),
        ('Credit card', 'Credit card'),
        ('Debit card', 'Debit card')
    )
    PAY_STATUS = (
        ('Pending', 'Pending'),
        ('Complete', 'Complete'),
        ('No Status', 'No Status')
        
    )
    VEHICLE_TYPE =(
        ('Semi-Trailer Truck', 'Semi-Trailer Truck'),
        ('Full-Trailer Truck', 'Full-Trailer Truck'),
        ('Jumbo-Trailer Truck ', 'Jumbo-Trailer Truck '),
        ('Tail-Lift Truck', 'Tail-Lift Truck')
    )
    
    
    vehicle_owner = models.CharField(max_length=60, null=False, blank=False)
    vehicle_address = models.TextField(max_length=400, null=False, blank=False)
    state = models.ForeignKey(State, on_delete=models.CASCADE, null=False, blank=False)
    city = models.ForeignKey(City, on_delete=models.CASCADE, null=False, blank=False)
    phone_regex = RegexValidator(regex=r'^[0-9]{10}', message="Phone number must be entered in the format: '999999999'. Up to 12 digits allowed.")
    mobile_number = models.CharField(validators=[phone_regex], max_length=13, null=False, blank=False, unique=True) # validators should be a list
    owner_email = models.EmailField(max_length=254, null=False, blank=False)
    pan = models.CharField(validators=[RegexValidator(r'^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$', message='PAN length has to be 10')],max_length=10, null=False, blank=False, unique=True)
    password = models.CharField(validators=[RegexValidator(r'^(\w+\d+|\d+\w+)+$', message='Password should be a combination of alphabets and numbers ')], max_length=40, null=False, blank=False)
    vehicle_availability=models.CharField(choices=AVAIL_STATUS,default='Available',max_length=25)
    vehicle_type = models.CharField(choices=VEHICLE_TYPE,default='Semi-Trailer Truck',max_length=25)
    vehicle_capacity = models.IntegerField(null=False, blank=False)
    #vehicle_return_datetime=models.DateTimeField('Estimated Return Date',default=timezone.now())
    amount_paid = models.DecimalField(max_digits=5, decimal_places=3,default=0.0)
    amount_pending = models.DecimalField(max_digits=5, decimal_places=3,default=0.0)
    payment_option = models.CharField(choices=PAYMENT_OPTION,default='Cash',max_length=25)
    status = models.CharField(choices=PAY_STATUS,default='No Status',max_length=25)
