from django.contrib import admin
from  .models import Customer, City, Area, State, Booking, Payment

# Register your models here.

admin.site.register(Customer)
admin.site.register(City)
admin.site.register(State)
admin.site.register(Area)
admin.site.register(Booking)
admin.site.register(Payment)